package com.nx.arch.transactionmsg.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @类名称 ConfigUtil.java
 * @类描述 配置辅助类
 * @作者  庄梦蝶殇 linhuaichuan@naixuejiaoyu.com
 * @创建时间 2020年4月12日 下午4:13:48
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年4月12日             
 *     ----------------------------------------------
 * </pre>
 */
public class ConfigUtil {
    
    private static final Logger log = LoggerFactory.getLogger(ConfigUtil.class);
    
    private static String defaultEnvPath = "/opt/system.env";
    
    public static final String SYS_ENV = "sys-env";
    
    public static final String TEST_SERVER = "testserver";
    
    public static String getServerSysEnv() {
        Properties properties = new Properties();
        try {
            InputStream in = new BufferedInputStream(new FileInputStream(defaultEnvPath));
            properties.load(in);
        } catch (IOException e) {
            log.info("load default /opt/system.env fail {}", e.getMessage());
        }
        String sysEnv = properties.getProperty(SYS_ENV);
        if (sysEnv == null || sysEnv.isEmpty()) {
            sysEnv = System.getenv(SYS_ENV);
        }
        return sysEnv;
    }
    
    public static boolean isTestEnv() {
        String sysEnv = getServerSysEnv();
        if (sysEnv != null && sysEnv.equals(TEST_SERVER)) {
            return true;
        }
        return false;
    }
    
    public static ServerType getServerType() {
        String sysEnv = getServerSysEnv();
        ServerType serverType = ServerType.getType(sysEnv);
        return serverType;
    }
    
    public static String parseJdbcUrl(String jdbcUrl) {
        if (jdbcUrl == null || jdbcUrl.isEmpty()) {
            return null;
        }
        String[] data = jdbcUrl.split("\\?");
        if (data == null || data.length == 0) {
            return null;
        }
        return data[0];
    }
    
    public static DbInfo getDataBase(String jdbcUrl) {
        if (jdbcUrl == null || jdbcUrl.isEmpty()) {
            return null;
        }
        String regexString = "^jdbc:mysql://(.*):(\\d*)/(.*)\\?.*$";
        String regexStringWithout = "^jdbc:mysql://(.*):(\\d*)/(.*).*$";
        String regex = regexString;
        if (!jdbcUrl.contains("?")) {
            regex = regexStringWithout;
        }
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(jdbcUrl);
        if (matcher.matches()) {
            DbInfo db = new DbInfo();
            String hostname = matcher.group(1);
            String port = matcher.group(2);
            String dabaBase = matcher.group(3);
            db.setHostname(hostname);
            db.setPort(port);
            db.setDataBase(dabaBase);
            return db;
        }
        return null;
    }
    
    /**
     * @类描述 数据库信息
     * @版本 1.0.0
     *
     * @修改记录
     * <pre>
     *     版本                       修改人 		修改日期 		 修改内容描述
     *     ----------------------------------------------
     *     1.0.0 		庄梦蝶殇 	2020年4月14日             
     *     ----------------------------------------------
     * </pre>
     */
    public static class DbInfo {
        String hostname;
        
        String port;
        
        String dataBase;
        
        public String getHostname() {
            return hostname;
        }
        
        public void setHostname(String hostname) {
            this.hostname = hostname;
        }
        
        public String getPort() {
            return port;
        }
        
        public void setPort(String port) {
            this.port = port;
        }
        
        public String getDataBase() {
            return dataBase;
        }
        
        public void setDataBase(String dataBase) {
            this.dataBase = dataBase;
        }
        
        public String buildKey() {
            String str = this.hostname + "_" + this.port + "_" + this.getDataBase();
            str = str.replace(".", "");
            return str;
        }
        
        @Override
        public String toString() {
            return "DB [hostname=" + hostname + ", port=" + port + ", dataBase=" + dataBase + "]";
        }
        
    }
    
    /**
     * @类描述 运行环境
     * @版本 1.0.0
     *
     * @修改记录
     * <pre>
     *     版本                       修改人 		修改日期 		 修改内容描述
     *     ----------------------------------------------
     *     1.0.0 		庄梦蝶殇 	2020年4月14日             
     *     ----------------------------------------------
     * </pre>
     */
    public static enum ServerType {
        /**
         * 生产环境
         */
        Online("online"),
        /**
         * 沙箱环境
         */
        Sandbox("sandbox"),
        /**
         * 测试环境
         */
        TestServer("testserver"),
        /**
         * 无环境
         */
        NONE("NONE");
        
        /**
         * 环境类型
         */
        private String type;
        
        ServerType(String type) {
            this.type = type;
        }
        
        public String getType() {
            return type;
        }
        
        public void setType(String type) {
            this.type = type;
        }
        
        public static ServerType getType(String typeName) {
            ServerType[] types = ServerType.values();
            for (ServerType type : types) {
                if (type.getType().equals(typeName)) {
                    return type;
                }
            }
            return ServerType.NONE;
        }
    }
}
